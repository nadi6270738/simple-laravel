<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class TaskController extends Controller
{
    /**
     * @return View
     */
    public function index()
    {
        $tasks = Http::get(env('API_URL') . '/')->json();
        return view('index', compact('tasks'));
    }

    /**
     * @param Request $request 
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $task = Http::post(env('API_URL') . '/', [
            'task' => $request['task'],
        ])->json();
        return response()->json($task);
    }

    /**
     * @param 
     * @return JsonResponse
     */
    public function update($id, Request $request): JsonResponse
    {
        $task = Http::put(env('API_URL') . '/' . $id, [
            'task' => $request['task'],
            'status' => $request['status'],
        ])->json();
        return response()->json($task);
    }

    /**
     * @param 
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $task = Http::delete(env('API_URL') . '/' . $id)->json();
        return response()->json($task);
    }
}
