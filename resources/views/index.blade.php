<!DOCTYPE html>
<html lang="en">
  <head>
    <link
      rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
      crossorigin="anonymous"/>
    <style>
      .app-container {
        height: 100vh;
        width: 100%;
      }
      .complete {
        text-decoration: line-through;
      }
    </style>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  </head>
  <body>
        <div
        class="app-container d-flex align-items-center justify-content-center flex-column"
        ng-app="myApp"
        ng-controller="myController">
        {{-- task_name --}}
        <h3>Todo App</h3>
        <div class="d-flex align-items-center mb-3">
            <div class="form-group mr-3 mb-0">
            <input
                ng-model="yourTask"
                type="text"
                class="form-control taskInput"
                id="formGroupExampleInput"
                placeholder="Enter a task here" />
            </div>
            <button
                type="button"
                class="btn btn-primary mr-3 submit-button"
                ng-click="saveTask()">Save</button>
            {{-- <button type="button" class="btn btn-warning" ng-click="getTask()">
            Get Tasks
            </button> --}}
        </div>
        {{-- yourName --}}
        <div class="table-wrapper">
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Todo item</th>
                        <th>status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody class="task-table">
                    @if($tasks['status'])
                        @php
                            $no = 0;
                        @endphp
                        @if(!empty($tasks['data']) && count($tasks['data']))
                            @foreach($tasks['data'] as $task)
                                <tr ng-repeat="task in tasks" id="row-{{ $task['_id'] }}" class="{{ $task['status'] == 'Selesai' ? 'table-success' : 'table-light' }}">
                                    <td>{{ ++$no }}</td>
                                    <td id="task-{{ $task['_id'] }}" class="{{ $task['status'] == 'Selesai' ? 'complete' : 'task' }}">{{ $task['task'] }}</td>
                                    <td id="status-{{ $task['_id'] }}">{{ $task['status']}}</td>
                                    <td>
                                        <button type="button" class="btn btn-danger delete-button" ng-click="delete" data-id="{{ $task['_id'] }}">Delete</button>
                                        <button type="button" class="btn btn-success update-button" ng-click="finished" data-id="{{ $task['_id'] }}">Finished</button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <script>
        $(function() {
            $('.submit-button').on('click', function() {
                // console.log('store')
                const input = $('.taskInput').val();
                // console.log(input)
                $.ajax({
                    url: "{{ url('tasks') }}",
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "task": input,
                    },
                    success: function(result) {
                        console.log(result)
                        console.log(result.data._id)
                        $('.taskInput').val('');
                        $('.task-table').append('<tr ng-repeat="task in tasks" class="table-light" id="row-' + result.data._id + '">' +
                                    '<td>{{ ++$no }}</td>' +
                                    '<td id="task-' + result.data._id + '" class="task">' + result.data.task + '</td>' +
                                    '<td id="status-' + result.data._id + '">' + result.data.status + '</td>' +
                                    '<td>' +
                                        '<button type="button" class="btn btn-danger delete-button" ng-click="delete" data-id="' + result.data._id + '">Delete</button>' +
                                        '<button type="button" class="btn btn-success update-button" ng-click="finished" data-id="' + result.data._id + '">Finished</button>' +
                                    '</td>' +
                                '</tr>')
                    }
                })
            });

            $('.update-button').on('click', function() {
                // console.log('update')
                const id = $(this).data('id');
                // console.log(id)
                const task = $('#task-' + id).html()
                // console.log(task)
                $.ajax({
                    url: "{{ url('tasks') }}" + '/' + id,
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "_method": "put",
                        "task": task,
                        "status": "Selesai",
                    },
                    success: function(result) {
                        $('#row-' + id).removeClass("table-light")
                        $('#row-' + id).addClass("table-success")
                        $('#task-' + id).removeClass("task")
                        $('#task-' + id).addClass("complete")
                        $('#status-' + id).html("Selesai")
                    }
                })
            });

            $('.delete-button').on('click', function() {
                // console.log('delete')
                const id = $(this).data('id');
                // console.log(id)
                $.ajax({
                    url: "{{ url('tasks') }}" + '/' + id,
                    method: 'POST',
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "_method": "delete",
                    },
                    success: function(task) {
                        $('#row-' + id).remove()
                    }
                })
            });
        });
    </script>
  </body>
</html>
